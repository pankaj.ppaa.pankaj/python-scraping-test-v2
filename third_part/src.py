#!/usr/bin/env python
# coding: utf-8

# Task 1:
# In the presence of a JSON file named third_part/categories.json.gz your task is to read its contents and
# build a recursive function capable of returning all categories and subcategories, mirroring the structure outlined in

# In[15]:


import json
import gzip
data=[]
def read_compressed_json(file_path):
    with gzip.open(file_path, 'rt', encoding='utf-8') as file:
        data = json.load(file)
    return data
file_path = 'Downloads/categories.json.gz'
json_data = read_compressed_json(file_path)

def extract_categories(data):
    categories = []

    for key, value in data.items():
        category = {'id': key}

        if 'i' in value and value['i']:
            category['i'] = extract_categories(value['i'])

        categories.append(category)

    return categories

result = extract_categories(json_data)
print(json.dumps(result, indent=2))


# Task 2:
# In the third_part folder, start a new scrapy project and build a spider that crawls the webpage below,
# extracting the products names, and the breadcrumb in a list format. Make sure to take
# advantage of Scrapy.Items to output the products.
# Test your spider and save the results in a csv file.
# Webpage:

# In[7]:


from bs4 import BeautifulSoup
import json
import re
import requests
import time
import datetime
import logging
import random
import sys
# reload(sys)
# sys.setdefaultencoding('utf8')
# from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from selenium.common.exceptions import NoSuchElementException
import os
from os import listdir
from unidecode import unidecode
#sys.setdefaultencoding('utf8')
#ffrom pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from unidecode import unidecode
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from asyncio.log import logger
import datetime
import os
import pandas as pd
import logging
import time
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
driver=webdriver.Chrome()
url='https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas'
driver.get(url)
zoom_level = 1.00
driver.execute_script(f"document.body.style.zoom = '{zoom_level}';")
time.sleep(1)
response=driver.page_source
breadcrumbs=driver.find_elements(By.CLASS_NAME,'breadcrumbs-link.fs.ng-star-inserted')
breadcrumbs_value=[f"{bread.text}"for bread in breadcrumbs]
print(breadcrumbs_value)


# Task-3

# In[17]:


from selenium import webdriver
from selenium.webdriver.common.by import By
driver=webdriver.Chrome()
url='https://www.edeka24.de/Lebensmittel/Suess-Salzig/Schokoriegel/'
driver.get(url)
try:
    pop_up=driver.find_element(By.CLASS_NAMEASS_NAME,'sc-dcJsrY.jXWxkg')
    pop_up.click()
except:
    pass
Data=[]
names=driver.find_elements(By.CLASS_NAME,'product-details')
for i in range(len(names)):
    name=names[i].find_element(By.TAG_NAME,'h2')
    Data.append(f'Name of product{i} : {name.text}')
breadcrumb=driver.find_element(By.CLASS_NAME,'breadcrumb')
bredcrumb=breadcrumb.text.split("\n")
print(bredcrumb)
print(Data)


# In[1]:





# In[ ]:




