#!/usr/bin/env python
# coding: utf-8

# Task1:
# Implement a function called http_request that sends a GET request to the following URL: 'https://eoa0qzkprh8wd6s.m.pipedream.net'.
# It is important to note that the expected response should contain a success message in JSON format. If the initial request does not yield a 200 response code along with the expected JSON response, please refine your request until it meets these criteria.

# In[3]:


import requests

url = 'https://eoa0qzkprh8wd6s.m.pipedream.net'

try:
    response = requests.get(url)
    if response.status_code == 200:
        try:
            json = response.json()
            print("Request successful")
            print(json)
        except ValueError:
            print("Unable to parse JSON response.")
    else:
        print(f"Unexpected response code: {response.status_code}")
except requests.RequestException as e:
    print(f"Error sending HTTP request: {e}")


# Task2:
# Given this curl, update it to make sure it will result in a successful HTTP 2XX response.

# In[16]:


curl -i -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36" https://eo6f3hp6twlkn24.m.pipedream.net/


# Task 3:
# 
# Write a function that obtains the date for the first day of the last week(not to be confused with previous week) in the current month.
# 
# Example: The last week if the current month is Feb 2024 would be from Feb 26 to 29
# 
# 
# Apply a decorator to this function so that it formats the date as 'yyyy-mm/d'.

# In[4]:


from datetime import datetime, timedelta

today_date = datetime.now()
first_day_of_month = today_date.replace(day=1)
days_until_first_monday = (first_day_of_month.weekday() - 0) % 7
first_monday_date = first_day_of_month + timedelta(days=days_until_first_monday)
last_day_of_month = (first_day_of_month.replace(month=first_day_of_month.month % 12 + 1, day=1) - timedelta(days=1))
remaining_days = (last_day_of_month - first_monday_date).days
remaining_weeks = remaining_days // 7
last_week_monday_date = last_day_of_month - timedelta(days=last_day_of_month.weekday())

print(f"The date is {last_week_monday_date.strftime('%Y-%m/%d')} on the first day of last week for the current month. ")


# Task 4:
# Write the tests for the class CacheDecorator without touching it, some of your tests should not pass because this class is a little buggy.

# In[ ]:


import unittest
from your_module import CacheDecorator

class TestCacheDecorator(unittest.TestCase):

    def test_cache_decorator_with_valid_input(self):
        @CacheDecorator
        def add(a, b):
            return a + b

        result = add(2, 3)
        self.assertEqual(result, 5)

        result = add(2, 3)
        self.assertEqual(result, 5)

    def test_cache_decorator_with_different_arguments(self):
        @CacheDecorator
        def add(a, b):
            return a + b
        result = add(2, 3)
        self.assertEqual(result, 5)

        result = add(4, 5)
        self.assertEqual(result, 9)

    def test_cache_decorator_with_mutable_arguments(self):
        @CacheDecorator
        def concat(lst):
            return ''.join(lst)

        result = concat(['a', 'b'])
        self.assertEqual(result, 'ab')
        result = concat(['a', 'b'])
        self.assertEqual(result, 'ab')

    def test_cache_decorator_with_invalid_input(self):
        @CacheDecorator
        def divide(a, b):
            return a / b
        result = divide(10, 2)
        self.assertEqual(result, 5)
        with self.assertRaises(ZeroDivisionError):
            divide(10, 0)

if __name__ == '__main__':
    unittest.main()


# In[ ]:




