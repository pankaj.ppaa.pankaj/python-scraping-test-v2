#!/usr/bin/env python
# coding: utf-8

# Given a list of keywords, find the most recurrent sequence of characters that appears in these keywords.
# for example ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog'] the result value should be either at or ho.
# Knowing that you shouldn't use any other list to store the sequences separately.

# In[41]:


keywords = ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog']
pairs={}
general_pairs=[]
for keys in keywords:
    for i in range(len(keys)-1):
        for j in range(i+1,i+2):
            general_pairs.append(f'{keys[i]}{keys[j]}')
count_pairs={}
for count in general_pairs:
    if count in count_pairs:
        count_pairs[count]+=1
    else:
        count_pairs[count]=1
max_value= max(count_pairs.values())
for key,values in count_pairs.items():
    if values==max_value:
        pairs[key]=values
print(pairs)


# Task2
# Given a text containing nutritional values and other properties of a product, Build a dict, having nutrition names as keys and nutrition values including the unit as dict values.

# In[18]:


import regex as re
text = "Additifs nutritionnels: Vitamine C-D3: 160 UI, Fer (3b103): 4mg, Iode (3b202): 0,28 mg, Cuivre (3b405, 3b406): 2,2 mg, Manganèse (3b502, 3b503, 3b504): 1,1 mg, Zinc (3b603,3b605, 3b606): 11 mg –Clinoptilolited’origine sédimentaire: 2 g. Protéine: 11,0% - Teneur en matières grasses: 4,5% - Cendres brutes: 1,7% - Cellulose brute: 0,5% - Humidité: 80,0%."
text=text.split(',')
a=text[0].split(':')
a=a[1:]
b=''
for i in a:
    b+=f'{i}:'
b=b[:-2]
text=text[1:]
text.insert(0,b)
composition=[[i] for i in text]

for inner_list in composition:
    if inner_list:
        inner_list[0] = inner_list[0].lstrip()
nutrition_dict = {}

for entry in composition:
    if entry:
        match = re.match(r'^(.*?):\s*([\d.]+)\s*([^\d]*)$', entry[0])
        if match:
            name, value, unit = match.groups()
            nutrition_dict[name.strip()] = {'value': float(value), 'unit': unit.strip()}

print(nutrition_dict)




# Task3:
# Create a function to check if a list is a grandma list or not, a grandma list is a list of numbers having the product of two or more adjacent numbers in one of its nested lists.
# Keep in mind that numbers could only be adjacent if they belong to the same innermost list. In the first example below, the first two numbers(1,2) are adjacent but the first three(1,2,4) aren't.# 

# In[115]:


def flat_list(arg):
    flat = []
    for i in arg:
        if isinstance(i, list):
            flat.extend(flat_list(i))
        elif isinstance(i, int):
            flat.append(i)
    return flat

def flatten_inner_lists(lst):
    result = []
    for item in lst:
        if isinstance(item, list):
            result.extend(item)
    return result

def is_grandma_list(arg):
    flat = flat_list(arg)
    print(flat)
    for j in range(len(flat) - 1):
        value = 1
        value *= flat[j]
        for k in range(j+1, len(flat)-1):
            value *= flat[k]
            if value in flat[k:]:
                print(value)
                return "It's a grandma list"
    return "Not a grandma list"

list1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
list2 = [5, 9, 4, [[8, 7]], 4, 7, 1, [[5]]]
result = is_grandma_list(list1)
result2 = is_grandma_list(list2)
print(result)
print(result2)

            
                    
                              



